#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import time

libs = ["openpyxl","pandas"]
try:
    import openpyxl
    import pandas as pd
except ImportError:
    for lib in libs:
        os.system("pip install " + lib)
    import openpyxl
    import pandas as pd

userPos = sys.argv[1]
fils_xlsx = userPos[:-4] + '-SMT坐标'+'.xlsx'
fils_csv = userPos[:-4] + '-SMT坐标'+'.csv'

try:
    posFile = open(userPos,mode="r")
    print('打开文件成功！')
except:
    print('打开文件失败，请检查文件位置及名称！')
    exit()

# 读取坐标文件内容
print('正在读取文件……')
posContent = posFile.readlines()
posData = []
for x in range(5, len(posContent)-1):
    posData.append(posContent[x].split())

posFile.close()

# 将数据写入xlsx
print('正在将数据写入文件……')
wb = openpyxl.Workbook()
sheet = wb.active

sheet.column_dimensions['A'].width = 10
sheet.column_dimensions['B'].width = 10
sheet.column_dimensions['C'].width = 30
sheet.column_dimensions['D'].width = 15
sheet.column_dimensions['E'].width = 15
sheet.column_dimensions['F'].width = 15
sheet.column_dimensions['G'].width = 10

#sheet.merge_cells('A1:K1')
#sheet['A1'] = posContent[2][3:]

sheet['A1'] = 'Designator'
sheet['B1'] = 'Footprint'
sheet['C1'] = 'Comment'
sheet['D1'] = 'Mid X'
sheet['E1'] = 'Mid Y'
sheet['F1'] = 'Rotation'
sheet['G1'] = 'Layer'
sheet['H1'] = 'Ref X'
sheet['I1'] = 'Ref Y'
sheet['J1'] = 'Pad X'
sheet['K1'] = 'Pad Y'

for x in range(len(posData)):
    sheet['A'+str(x+2)] = posData[x][0]
    sheet['B'+str(x+2)] = posData[x][1]
    sheet['C'+str(x+2)] = posData[x][2]
    sheet['D'+str(x+2)] = posData[x][3]
    sheet['E'+str(x+2)] = posData[x][4]
    sheet['F'+str(x+2)] = posData[x][5]
    sheet['G'+str(x+2)] = posData[x][6]

wb.save(fils_xlsx)
wb.close()
print('完成写入并生成文件！')

tmp = pd.read_excel(fils_xlsx,index_col=0)
tmp.to_csv(fils_csv,encoding='utf-8')
os.remove(fils_xlsx)
exit()
