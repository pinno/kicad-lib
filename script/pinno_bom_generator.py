#!/usr/bin/python
#coding:utf-8
#
# Example python script to generate a BOM from a KiCad generic netlist
#
# Example: Sorted and Grouped CSV BOM
#

"""
    @package
    Generate a Tab delimited list (csv file type).
    Components are sorted by ref and grouped by value with same footprint
    Fields are (if exist)
    'Reference', 'Quantity', 'Value', 'Cmp name', 'Footprint', 'Manufacturer', 'PartNumber', 'Supplier', 'Sku','Price', 'Description', 'Datasheet'

    Command line:
    python "pathToFile/pinno_script_bom_csv_grouped_by_value_with_footprint.py" "%I" "%O"
"""

# Import the KiCad python helper module and the csv formatter
import sys
import os
import kicad_netlist_reader
import csv
import string

#libs = ["pandas"]

#try:
#    import pandas as pd
#except ImportError:
#    for lib in libs:
#        os.system("pip install " + lib)
#    import pandas as pd


# Generate an instance of a generic netlist, and load the netlist tree from
# the command line option. If the file doesn't exist, execution will stop
net = kicad_netlist_reader.netlist(sys.argv[1])

bom_csv  = sys.argv[2] + "-BOM.csv"
bom_xlsx = sys.argv[2] + "-BOM.xlsx"
# Open a file to write to, if the file cannot be opened output to stdout
# instead
try:
    f = open(bom_csv, 'w')
except IOError:
    e = "Can't open output file for writing: " + sys.argv[2]
    print(__file__, ":", e, sys.stderr)
    f = sys.stdout

# Create a new csv writer object to use as the output formatter
out = csv.writer(f, lineterminator='\n', delimiter=',', quotechar='\"', quoting=csv.QUOTE_ALL)

# Output a set of rows for a header providing general information
#out.writerow(['Source:', net.getSource()])
#out.writerow(['Date:', net.getDate()])
#out.writerow(['Tool:', net.getTool()])
#out.writerow( ['Generator:', sys.argv[0]] )
#out.writerow(['Component Count:', len(net.components)])
out.writerow(['类别', '值', '封装', '数量', '制造商', '型号', '描述', '位号', '供应商', '编号',  '单价', '规格书'])

# Get all of the components in groups of matching parts + values
# (see ky_generic_netlist_reader.py)
grouped = net.groupComponents()

# Output all of the component information
for group in grouped:
    refs = ""

    # Add the reference of every component in the group and keep a reference
    # to the component so that the other data can be filled in once per group
    for component in group:
        refs += component.getRef() + ', '
        c = component

    # Fill in the component groups common data
    strValue = c.getValue()
    if strValue.find('DNP')<0:
        out.writerow([c.getProperty("ki_keywords"), c.getValue(), c.getFootprint(), len(group), c.getField("制造商"), c.getField("型号"), c.getDescription(), refs, c.getField("供应商"), c.getField("供应商料号"),c.getField("单价"), c.getDatasheet()])
    else:
        print("exclude component :" + refs)
#pd read csv file
#tmp = pd.read_csv(bom_csv, encoding='utf-8', converters={'位号':str,'数量':str,'值':str,'封装':str,'制造商':str,'型号':str,'供应商链##接':str,'编号':str,'单价':str,'描述':str,'规格书':str})
#tmp.to_excel(bom_xlsx, sheet_name="sheet1", index=None)
#os.remove(bom_csv)
